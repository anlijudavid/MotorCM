package motorj;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author julian david m r
 */
public class MotorJ {

    private static final Temporizador t = new Temporizador();

    public static void main(String[] args) {
        String compilar = null, respuesta = null, tiempo_max = null;

        //Analizar los argumentos de entrada
        for (int i = 0; i < args.length; i++) {
            System.out.println("Analizando " + args[i]);
            switch (args[i]) {
                case "-compilar":
                    compilar = args[i + 1];
                    break;
                case "-respuesta":
                    respuesta = args[i + 1];
                    break;
                case "-tiempo_max":
                    tiempo_max = args[i + 1];
                    break;
            }
        }

        if (respuesta != null && compilar != null) {
            System.out.println("Compilar: " + compilar + "\tRespuesta: " + respuesta);

            //Compilacion
            System.out.println("\nRESULTADO DE COMPILACION:\t" + new ManejadorArchivos().Compilar(compilar));

            //Compilar
            
            //Iniciar temporizador
            t.Iniciar();
            
            // Algoritmo de prueba
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MotorJ.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            // Parar
            t.Parar();
            
            //Mostrando resultados
            System.out.println(t);
        } else {
            System.out.println("No hay argumentos de entrada");
        }
    }

}
