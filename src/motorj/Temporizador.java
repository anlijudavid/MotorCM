package motorj;

/**
 * Medicion de tiempo preciso
 *
 * @author julian
 */
public class Temporizador {

    private long time_inicio, time_fin;
    private long nanos, milisegundos, segundos, minutos;

    public Temporizador() {
        nanos = milisegundos = segundos = minutos = -1;
    }

    public long Iniciar() {
        return time_inicio = System.nanoTime();
    }

    public long Parar() {
        return time_fin = System.nanoTime();
    }

    public long getTiempoTotal() {
        return (time_fin - time_inicio);
    }

    public long getNanos() {
        return nanos = getTiempoTotal();
    }

    public long getMilisegundos() {
        return milisegundos = getTiempoTotal() / 1000000;
    }

    public long getSegundos() {
        return segundos = getMilisegundos() / 1000;
    }

    public long getMinutos() {
        return minutos = getSegundos() / 60;
    }

    private void Ajustar() {
        getNanos();
        getMilisegundos();
        getSegundos();
        getMinutos();

        nanos = ((nanos - (milisegundos * 1000000)));
        milisegundos = ((milisegundos - (segundos * 1000)));
    }

    @Override
    public String toString() {
        Ajustar();
        return "Temporizador{"
                + "nanos=" + nanos
                + ", milisegundos=" + milisegundos
                + ", segundos=" + segundos
                + ", minutos=" + minutos
                + '}';
    }

}
