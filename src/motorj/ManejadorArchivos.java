package motorj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Clase dedicada a Compilar archivos
 *
 * @author julian
 */
public class ManejadorArchivos {

    private final String cmd_compilar_archivo = "cd ../Prueba && sh compile.sh";

    /**
     * Compilador de solamente .java
     *
     * @param archivo
     * @return
     */
    public String Compilar(String archivo) {
        if (ValidarExtensionFichero(archivo)) {
            return "El archivo a compilar no pertenece a una extension valida.";
        }
        ArrayList<String> resultados = new ArrayList();
        return EjecutarComando(cmd_compilar_archivo, resultados);
    }

    /**
     * ejecucion de solamente .java
     *
     * @param archivo: Nombre del archivo, no necesariamente el nombre sino que
     * la direccion que incluya al final el nombre y extension del archivo a
     * compilar
     * @param archivo_casosPrueba, Nombre del archivo txt, no necesariamente el
     * nombre sino que la direccion que incluya al final el nombre y extension
     * del archivo a compilar
     * @return Resultados de la operacion a ejecutar
     */
    public String Correr(String archivo, String archivo_casosPrueba) {
        ArrayList<String> resultados = new ArrayList();
        return EjecutarComando("cd ../Prueba && sh compile.sh "
                + archivo
                + " "
                + archivo_casosPrueba, resultados
        );
    }

    /**
     *
     * @param comando: Comando a ejecutar en la shell linux
     * @param resultados: Variable que almacena cada resultado en un vector
     * @return resultados de la operacion
     */
    public String EjecutarComando(String comando, ArrayList<String> resultados) {
        Process process;
        String line, salida = "";
        try {
            process = Runtime.getRuntime().exec(comando);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            try {
                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                    if (resultados != null) {
                        resultados.add(line);
                    }
                    salida += line;
                }
            } catch (IOException ex) {
                System.out.println("Error: " + ex.getCause().getMessage());
                return "El comando se ejecuto pero no se pudieron obtener los resultados";
            }
        } catch (IOException ex) {
            return "No se ejecuto el comando / error en la ejecucion";
        }
        return salida;
    }

    public Boolean ValidarExtensionFichero(String nombre) {
        return nombre.endsWith(".java") || nombre.endsWith(".c") || nombre.endsWith(".cpp");
    }
}
