package motorj;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Leer el archivo
 * @author julian
 */
public class Lectura {

    public void mostrarContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while ((cadena = b.readLine()) != null) {
            System.out.println(cadena);
        }
        b.close();
    }

    public ArrayList<String> getContenido(String dir_archivo) throws FileNotFoundException, IOException {
        String cadena;
        ArrayList<String> cont = new ArrayList();
        FileReader f = new FileReader(dir_archivo);
        BufferedReader b = new BufferedReader(f);
        while ((cadena = b.readLine()) != null) {
            cont.add(cadena);
        }
        b.close();        
        return cont;
    }
}
